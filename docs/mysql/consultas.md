# Consultas MySQL

```mysql hl_lines="13"
SELECT
  Employees.EmployeeID,
  Employees.Name,
  Employees.Salary,
  Manager.Name AS Manager
FROM
  Employees
LEFT JOIN
  Employees AS Manager
ON
  Employees.ManagerID = Manager.EmployeeID
WHERE
  Employees.EmployeeID = '087652';
```

## Admonition

!!! tip "Dica"
    sdsadasadsa dasdsa


!!! danger "Cuidado"
    sdsadasadsa dasdsa


!!! note "Lembrete"
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.